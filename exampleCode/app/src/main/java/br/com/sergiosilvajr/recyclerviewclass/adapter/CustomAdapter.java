package br.com.sergiosilvajr.recyclerviewclass.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.sergiosilvajr.recyclerviewclass.R;

/**
 * Created by luissergiodasilvajunior on 19/11/16.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ExampleViewHolder> {
    List<Integer> numbers = new ArrayList<>();

    public CustomAdapter(List<Integer> list){
        numbers = list;
    }

    public CustomAdapter(){}

    public void addNewInteger(Integer integer){
        numbers.add(integer);
    }

    @Override
    public ExampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
        return new ExampleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ExampleViewHolder holder, int position) {
        Integer current = numbers.get(position);
        holder.textView.setText(String.valueOf(current));
    }

    @Override
    public int getItemCount() {
        return numbers.size();
    }

    public static class ExampleViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView;
        public ExampleViewHolder(View view){
            super(view);

            imageView = (ImageView) view.findViewById(R.id.image_view);
            textView = (TextView) view.findViewById(R.id.text_view);
        }
    }
}
