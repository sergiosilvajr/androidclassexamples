package br.com.sergiosilvajr.recyclerviewclass;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.sergiosilvajr.recyclerviewclass.R;
import br.com.sergiosilvajr.recyclerviewclass.adapter.CustomAdapter;
import br.com.sergiosilvajr.recyclerviewclass.db.Contact;
import br.com.sergiosilvajr.recyclerviewclass.db.DBHelper;

public class DBActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DBHelper dbHelper = new DBHelper(this);
        dbHelper.addContact(new Contact("Ravi", "144"));
        dbHelper.addContact(new Contact("Souza", "911"));
        dbHelper.addContact(new Contact("Álvaro", "190"));

        Log.d("Reading: ", "Reading all contacts..");
        List<Contact> contacts = dbHelper.getAllContacts();

        for (Contact cn : contacts) {
            String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
            // Writing Contacts to log
            Log.d("Name: ", log);
        }

       }


}
