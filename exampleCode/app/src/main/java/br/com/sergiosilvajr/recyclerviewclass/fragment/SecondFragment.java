package br.com.sergiosilvajr.recyclerviewclass.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.sergiosilvajr.recyclerviewclass.R;

/**
 * Created by luissergiodasilvajunior on 18/11/16.
 */

public class SecondFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.second_fragment_view_pager, container, false);

        return root;
    }
}
