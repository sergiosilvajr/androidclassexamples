package br.com.sergiosilvajr.recyclerviewclass.db;

/**
 * Created by luissergiodasilvajunior on 19/11/16.
 */
public class Contact {
    int id;
    String name;
    String phoneNumber;

    public Contact(int id, String name, String phoneNumber){
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }
    public Contact(){}

    public Contact(String name, String phoneNumber){
        this.name = name;
        this.phoneNumber = phoneNumber;
    }
    public int getID(){
        return this.id;
    }

    public void setID(int id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getPhoneNumber(){
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phone_number){
        this.phoneNumber = phone_number;
    }
}
