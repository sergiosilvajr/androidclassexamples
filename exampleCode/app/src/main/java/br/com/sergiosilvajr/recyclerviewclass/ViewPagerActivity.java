package br.com.sergiosilvajr.recyclerviewclass;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import br.com.sergiosilvajr.recyclerviewclass.adapter.ViewPagerAdapter;
import br.com.sergiosilvajr.recyclerviewclass.fragment.FirstFragment;
import br.com.sergiosilvajr.recyclerviewclass.fragment.SecondFragment;

/**
 * Created by luissergiodasilvajunior on 19/11/16.
 */

public class ViewPagerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vp);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getFragmentList(), getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

   public List<Fragment> getFragmentList(){
       List<Fragment> fragments= new ArrayList<>();
       fragments.add(new FirstFragment());
       fragments.add(new SecondFragment());

       return fragments;
   }
}
