package br.com.sergiosilvajr.recyclerviewclass;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.sergiosilvajr.recyclerviewclass.adapter.CustomAdapter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_items);
        final CustomAdapter customAdapter =  new CustomAdapter(initList(5));

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                customAdapter.addNewInteger(new Random().nextInt(1000));
                customAdapter.notifyDataSetChanged();
            }
        });


        recyclerView.setAdapter(customAdapter);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
    }

    private List<Integer> initList(int max){
        List<Integer> numbers =  new ArrayList<>();
        for(int i = 0; i <max; i++){
            numbers.add(i);
        }
        return numbers;
    }
}
