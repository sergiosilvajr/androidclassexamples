package br.com.sergiosilvajr.recyclerviewclass.prefs;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by luissergiodasilvajunior on 19/11/16.
 */

public final class Prefs {
    private static final String SHARED_PREFS = "SHARED_PREFS";

    public static void putString(Context context, String key, String value){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public static String readString(Context context, String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "none");
    }
}
